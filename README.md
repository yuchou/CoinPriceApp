# 相关技术
1. swift + Moya + SwiftyJSON等

# 功能列表
## 已完成功能
1. 显示top100的数字货币行情(仅支持人民币和美元数据)
2. 支持搜索和实时刷新功能
## To do
1. 数据可视化
2. 消息推送
3. ...

# api接口
> https://api.coinmarketcap.com/v2

