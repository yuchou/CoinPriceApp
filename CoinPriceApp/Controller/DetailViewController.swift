//
//  DetailViewController.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/13.
//  Copyright © 2018 yu chou. All rights reserved.
//
import GoogleMobileAds
import UIKit
import SwiftyJSON
import SnapKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tickerId: Int!
    var tickerDetail = [TickerModel]()
    var tableView: UITableView!
    let detailCell = "DetailCell"
    var timer: Timer!
    var swipe: UISwipeGestureRecognizer!
    
    lazy var bannerView: GADBannerView = {
        let bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.adUnitID = "ca-app-pub-4328530438973315/2841660650"
        bannerView.delegate = self
        bannerView.rootViewController = self
        
        return bannerView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupViews()
        setupads()
        loadDatas()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // 定时器取消注册
        timer.invalidate()
    }
    
    func setup() {
        view.backgroundColor = .white
        let shareButton = UIBarButtonItem(image: UIImage(named: "share"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(sharePressed))
        navigationItem.rightBarButtonItem = shareButton
        
        swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipePressed))
        swipe.direction = .right
        view.addGestureRecognizer(swipe)
    }
    
    // 广告注册
    func setupads() {
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID, "ca4901f4a74970e2cfddc051b9e3fbcf" ]
        bannerView.load(request)
    }
    
    func setupViews() {
        tableView = UITableView(frame: self.view.frame, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        // 关闭屏幕滚动
        tableView.isScrollEnabled = false
        view.addSubview(tableView)
        self.view.showWait("loadTitle".localized)
        
        // 定时器注册
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateDatas), userInfo: nil, repeats: true)
    }
    
    func loadDatas() {
        CoinMarkerProvier.request(.tickerid(id: tickerId!)) { result in
            switch result {
            case let .success(response):
                let data = try? response.mapJSON()
                let json = JSON(data!)
                let dat = json["data"].arrayValue
                self.tickerDetail.append(TickerModel(jsonData: dat[0]))
                
                DispatchQueue.main.async {
                    // 手动隐藏消息框
                    self.view.hideWaitInfo()
                    self.tableView.reloadData()
                }
                
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func updateDatas() {
        loadDatas()
    }
    
    // MARK: 右滑手势逻辑
    @objc func swipePressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: 系统自带分享逻辑
    @objc func sharePressed() {
        let image = screenShot()
        let shareText = "shareTitle".localized
        let activities: [UIActivity] = []
        let vc = UIActivityViewController(activityItems: [shareText, image!], applicationActivities: activities)
        vc.excludedActivityTypes = [.addToReadingList, .addToReadingList, .markupAsPDF, .openInIBooks,
                                    .postToFlickr, .postToVimeo, .postToTencentWeibo, .postToWeibo]
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(vc, animated: true)
    }
    
    // MARK: 截图当前屏幕逻辑
    func screenShot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: .value1, reuseIdentifier: detailCell)
        if tickerDetail.count > 0 {
            if indexPath.row == 0 {
                cell.textLabel?.text = "coinName".localized
                cell.detailTextLabel?.text = "\(tickerDetail[0].name)"
            }
            
            if indexPath.row == 1 {
                cell.textLabel?.text = "coinSymbol".localized
                cell.detailTextLabel?.text = "\(tickerDetail[0].symbol)"
            }
            
            if indexPath.row == 2 {
                cell.textLabel?.text = "coinRank".localized
                cell.detailTextLabel?.text = "\(tickerDetail[0].rank)"
            }
            
            if indexPath.row == 3 {
                cell.textLabel?.text = "coinUDSPrice".localized
                cell.detailTextLabel?.text = "$\(String(format: "%.2f", tickerDetail[0].quotes_usd.price))"
            }
            
            if indexPath.row == 4 {
                cell.textLabel?.text = "coinCNYPrice".localized
                cell.detailTextLabel?.text = "¥\(String(format: "%.2f", tickerDetail[0].quotes_cny.price))"
            }
            
            if indexPath.row == 5 {
                let percent = tickerDetail[0].quotes_usd.percent_change_1h
                cell.textLabel?.text = "coinChange1h".localized
                cell.detailTextLabel?.text = "\(percent)%"
                if percent > Double(0) {
                    cell.detailTextLabel?.textColor = UIColor(red:0.12, green:0.68, blue:0.30, alpha:1.0)
                } else {
                    cell.detailTextLabel?.textColor = .red
                }
            }
            
            if indexPath.row == 6 {
                let percent = tickerDetail[0].quotes_usd.percent_change_24h
                cell.textLabel?.text = "coinChange24h".localized
                cell.detailTextLabel?.text = "\(percent)%"
                if percent > Double(0) {
                    cell.detailTextLabel?.textColor = UIColor(red:0.12, green:0.68, blue:0.30, alpha:1.0)
                } else {
                    cell.detailTextLabel?.textColor = .red
                }
            }
            
            if indexPath.row == 7 {
                let percent = tickerDetail[0].quotes_usd.percent_change_7d
                cell.textLabel?.text = "coinChange7d".localized
                cell.detailTextLabel?.text = "\(percent)%"
                if percent > Double(0) {
                    cell.detailTextLabel?.textColor = UIColor(red:0.12, green:0.68, blue:0.30, alpha:1.0)
                } else {
                    cell.detailTextLabel?.textColor = .red
                }
            }
            
            if indexPath.row == 8 {
                cell.textLabel?.text = "coinAvailableSupply".localized
                cell.detailTextLabel?.text = "\(tickerDetail[0].circulating_supply)"
            }
            
            if indexPath.row == 9 {
                cell.textLabel?.text = "coinTotalSupply".localized
                cell.detailTextLabel?.text = "\(tickerDetail[0].total_supply)"
            }
            
            if indexPath.row == 10 {
                cell.textLabel?.text = "coinMarketCapUSD".localized
                cell.detailTextLabel?.text = "$\(tickerDetail[0].quotes_usd.market_cap)"
            }
            
            if indexPath.row == 11 {
                cell.textLabel?.text = "coinMarketCapCNY".localized
                cell.detailTextLabel?.text = "¥\(tickerDetail[0].quotes_cny.market_cap)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: 自定义headerView的高度
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    // MARK: 自定义headerView
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = DetailView(frame: CGRect(x:0, y:0, width:self.tableView.bounds.width, height:80))
        headerView.symbolImageView?.image = UIImage(named: "64x64/\(tickerId!)")
        return headerView
    }
    
    // Google广告栏
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return bannerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return bannerView.frame.height
    }
}


// MARK: Google广告
extension DetailViewController: GADBannerViewDelegate {
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
