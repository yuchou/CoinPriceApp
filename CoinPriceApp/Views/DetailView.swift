//
//  DetailView.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/18.
//  Copyright © 2018 yu chou. All rights reserved.
//

import UIKit
import SnapKit

class DetailView: UIView {
    var symbolImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        setupViews()
    }
    
    func setup() {
        symbolImageView = UIImageView()
        symbolImageView.contentMode = .scaleAspectFill
        addSubview(symbolImageView)
    }
    
    func setupViews() {
        self.symbolImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalTo(64)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
