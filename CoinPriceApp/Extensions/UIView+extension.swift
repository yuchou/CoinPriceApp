//
//  MBProgressHUD+extension.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/23.
//  Copyright © 2018 yu chou. All rights reserved.
//

import Foundation
import MBProgressHUD

extension UIView {
    // 当成功、失败的提示显示后，经过 1 秒钟会自动隐藏消失。而普通消息提示和等待提示显示后不会自动取消，需要手动将其隐藏。
    // 显示等待消息
    func showWait(_ title: String) {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.label.text = title
        hud.removeFromSuperViewOnHide = true
    }
    
    // 显示普通消息
    func showInfo(_ title: String) {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.mode = .customView  //模式设置为自定义视图
        hud.customView = UIImageView(image: UIImage(named: "info")!)
        hud.label.text = title
        hud.removeFromSuperViewOnHide = true
    }
    
    // 手动隐藏消息
    func hideWaitInfo() {
        MBProgressHUD.hide(for: self, animated: true)
    }
    
    // 显示成功消息
    func showSuccess(_ title: String) {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.mode = .customView
        hud.customView = UIImageView(image: UIImage(named: "tick")!)
        hud.label.text = title
        hud.removeFromSuperViewOnHide = true
        hud.hide(animated: true, afterDelay: 1)
    }
    
    // 显示失败消息
    func showError(_ title: String) {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.mode = .customView
        hud.customView = UIImageView(image: UIImage(named: "cross")!)
        hud.label.text = title
        hud.removeFromSuperViewOnHide = true
        hud.hide(animated: true, afterDelay: 1)
    }
}
