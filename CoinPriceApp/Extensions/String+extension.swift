//
//  Language.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/24.
//  Copyright © 2018 yu chou. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
