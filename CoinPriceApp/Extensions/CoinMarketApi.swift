//
//  CoinMarketApi.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/13.
//  Copyright © 2018 yu chou. All rights reserved.
//

import Foundation
import Moya

let CoinMarkerProvier = MoyaProvider<CoinMarker>()

public enum CoinMarker {
    case listings
    case tickers
    case tickerid(id: Int)
}

extension CoinMarker: TargetType {

    public var baseURL: URL {
        return URL(string: "https://api.coinmarketcap.com/v2")!
    }
    
    public var path: String {
        switch self {
        case .listings:
            return "/listings/"
        case .tickers:
            return "/ticker/"
        case .tickerid(let id):
            return "/ticker/\(id)/"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        switch self {
        case .listings:
            return .requestPlain
        case .tickers:
            return .requestParameters(parameters: ["sort": "rank", "structure": "array", "convert": "CNY"], encoding: URLEncoding.default)
        case .tickerid(_):
            return .requestParameters(parameters: ["structure": "array", "convert": "CNY"], encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return nil
    }
    
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
}



