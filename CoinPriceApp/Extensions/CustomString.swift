//
//  CustomString.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/19.
//  Copyright © 2018 yu chou. All rights reserved.
//

import UIKit

public func CustomString(fulltext: String, text: String, color: String) -> NSMutableAttributedString {
    let attrString: NSMutableAttributedString = NSMutableAttributedString(string: fulltext)
    let str = NSString(string: fulltext)
    let theRange = str.range(of: text)
    if color == "red" {
        attrString.addAttribute(.foregroundColor, value: UIColor.red, range: theRange)
    } else {
        attrString.addAttribute(.foregroundColor, value: UIColor(red:0.12, green:0.68, blue:0.30, alpha:1.0), range: theRange)
    }
    return attrString
}
