//
//  ViewController.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/13.
//  Copyright © 2018 yu chou. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyJSON
import SnapKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let coinCell = "CoinCell"
    var tableView: UITableView!
    var tickerModel = [TickerModel]()
    var tickerResults = [TickerModel]()
    var timer: Timer!
    var currencyButton: UIButton!
    var isClick: Bool = false
    var fulltext: String = ""
    var price: String = ""
    var percent: Double = 0.0

    let searchController = UISearchController(searchResultsController: nil)
    lazy var bannerView: GADBannerView = {
        let bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.adUnitID = "ca-app-pub-4328530438973315/2841660650"
        bannerView.delegate = self
        bannerView.rootViewController = self
        
        return bannerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupViews()
        setupads()
        loadDatas()
    }
    
    // 广告注册
    func setupads() {
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID, "ca4901f4a74970e2cfddc051b9e3fbcf" ]
        bannerView.load(request)
    }
    
    func setup() {
        title = "mainTitle".localized
        view.backgroundColor = .white
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = "searchPlaceholder".localized
        searchController.searchBar.tintColor = .white
        searchController.searchResultsUpdater = self
        // 设置开始搜索时导航条是否隐藏
        searchController.hidesNavigationBarDuringPresentation = false
        // 设置开始搜索时背景是否显示
        searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        
        currencyButton = UIButton()
        currencyButton.setImage(UIImage(named: "usd2"), for: .normal)
        currencyButton.addTarget(self, action: #selector(currencyButtonPressed), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: currencyButton)
    }
    
    func setupViews() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshPressed), for: .valueChanged)
        
        tableView = UITableView(frame: self.view.frame, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        // 使用自定义UITableView表格样式
        // tableView.register(UITableViewCell.self, forCellReuseIdentifier: coinCell)
        tableView.refreshControl = refreshControl
        view.addSubview(tableView)
        self.view.showWait("loadTitle".localized)
        
        // 定时器注册
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateDatas), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // 定时器取消注册
        timer.invalidate()
    }
    
    func loadDatas() {
        CoinMarkerProvier.request(.tickers) { result in
            switch result {
            case let .success(response):
                let data = try? response.mapJSON()
                let json = JSON(data!)
                let dat = json["data"].arrayValue
                var tmpModel = [TickerModel]()
                for i in 0..<dat.count {
                    tmpModel.append(TickerModel(jsonData: dat[i]))
                }
                
                // 根据rank的大小，顺序排列
                self.tickerModel = tmpModel.sorted() { $0.rank < $1.rank}

                DispatchQueue.main.async {
                    // 手动隐藏消息框
                    self.view.hideWaitInfo()
                    self.tableView.reloadData()
                }
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func updateDatas() {
        loadDatas()
    }
    
    @objc func refreshPressed(rf: UIRefreshControl) {
        loadDatas()
        rf.endRefreshing()
    }
    
    @objc func currencyButtonPressed(bt: UIButton) {
        if bt.currentImage == UIImage(named: "usd2") {
            bt.setImage(UIImage(named: "cny2"), for: .normal)
            isClick = true
            self.tableView.reloadData()
        } else {
            bt.setImage(UIImage(named: "usd2"), for: .normal)
            isClick = false
            self.tableView.reloadData()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchController.isActive ? tickerResults.count : tickerModel.count
    }
    
    // 可以这这里添加动画
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0
//        UIView.animate(withDuration: 2.0) {
//            cell.alpha = 1
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = tableView.dequeueReusableCell(withIdentifier: coinCell, for: indexPath)
        // 使用自定义UITableView表格样式
        let ticker = searchController.isActive ? tickerResults[indexPath.row] : tickerModel[indexPath.row]
        let cell: UITableViewCell = UITableViewCell(style: .value1, reuseIdentifier: coinCell)
        
        if isClick == true {
            price = String(format: "%.2f", ticker.quotes_cny.price)
            percent = ticker.quotes_cny.percent_change_24h
            fulltext = "¥\(price)(\(percent)%)"
        } else {
            price = String(format: "%.2f", ticker.quotes_usd.price)
            percent = ticker.quotes_usd.percent_change_24h
            fulltext = "$\(price)(\(percent)%)"
        }
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = "\(ticker.name)"
        cell.imageView?.image = UIImage(named: "16x16/\(ticker.id)")
        cell.detailTextLabel?.text = fulltext
        
        if percent > Double(0) {
            cell.detailTextLabel?.attributedText = CustomString(fulltext: fulltext, text: "\(percent)%", color: "green")
        } else {
            cell.detailTextLabel?.attributedText = CustomString(fulltext: fulltext, text: "\(percent)%", color: "red")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let ticker = searchController.isActive ? tickerResults[indexPath.row] : tickerModel[indexPath.row]
        let detailVC = DetailViewController()
        detailVC.tickerId = ticker.id
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    // Google广告栏
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return bannerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return bannerView.frame.height
    }
}

// MARK: 搜索结果处理
extension MainViewController: UISearchResultsUpdating {
    
    func filterContent(for searchText: String) {
        // 修改之前的绝对匹配的判断
        // tickerResults = tickerModel.filter { $0.name.lowercased() == searchText.lowercased() }
        // 设置结果为实时更新，localizedCaseInsensitiveContains: 本地化是否包含字符串(不区分大小写)
        tickerResults = tickerModel.filter { $0.name.localizedCaseInsensitiveContains(searchText) }
    }
    
    // 遵守UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContent(for: searchText)
            tableView.reloadData()
        }
    }
}

// MARK: Google广告
extension MainViewController: GADBannerViewDelegate {
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        let translateTransform = CGAffineTransform(translationX: 0, y: -bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        
        UIView.animate(withDuration: 0.5) {
            bannerView.transform = CGAffineTransform.identity
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
}
