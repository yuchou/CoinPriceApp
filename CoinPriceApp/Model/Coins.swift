//
//  Coins.swift
//  CoinPriceApp
//
//  Created by yu chou on 2018/7/16.
//  Copyright © 2018 yu chou. All rights reserved.
//

import Foundation
import SwiftyJSON

enum Currency: String {
    case usd = "USD"
    case cny = "CNY"
}

struct TickerModel {
    
    var id: Int
    var name: String
    var symbol: String
    var website_slug: String
    var rank: Int
    var circulating_supply: Double
    var total_supply: Double
    var max_supply: Double
    var quotes_usd: TickerQuotesModel
    var quotes_cny: TickerQuotesModel
    var last_updated: Int
    
    init(jsonData: JSON) {
        self.id = jsonData["id"].intValue
        self.name = jsonData["name"].stringValue
        self.symbol = jsonData["symbol"].stringValue
        self.website_slug = jsonData["website_slug"].stringValue
        self.rank = jsonData["rank"].intValue
        self.circulating_supply = jsonData["circulating_supply"].doubleValue
        self.total_supply = jsonData["total_supply"].doubleValue
        self.max_supply = jsonData["max_supply"].doubleValue
        self.quotes_usd = TickerQuotesModel(jsonData: jsonData["quotes"]["USD"])
        self.quotes_cny = TickerQuotesModel(jsonData: jsonData["quotes"]["CNY"])
        self.last_updated = jsonData["last_updated"].intValue
    }
}

struct TickerQuotesModel {
    var price: Double
    var volume_24h: Double
    var market_cap: Double
    var percent_change_1h: Double
    var percent_change_24h: Double
    var percent_change_7d: Double
    
    init(jsonData: JSON) {
        self.price = jsonData["price"].doubleValue
        self.volume_24h = jsonData["volume_24h"].doubleValue
        self.market_cap = jsonData["market_cap"].doubleValue
        self.percent_change_1h = jsonData["percent_change_1h"].doubleValue
        self.percent_change_24h = jsonData["percent_change_24h"].doubleValue
        self.percent_change_7d = jsonData["percent_change_7d"].doubleValue
    }
}
